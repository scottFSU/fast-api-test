***USER MODEL***
The user model will contain the following methods, paths, and endpoints.

Show a single user details:
    Endpoint path: /api/users/{ID}
    Endpoint method: GET
    Headers: authorization: Bearer token
    Response body: JSON, no body in the request.
		
    {
        "id": 1,
        "username": "Really Cool User",
        "password": "7099897654" (encrypted), 
    }

Create a user:
    Endpoint path: /api/users
    Endpoint method: POST
    Request body: JSON
		
    {
        "username": "Really Cool User",
    }

    Response: Status 200
    Response body: JSON

    {
        "id": 1,
        "username": "Really Cool User",
    }


***WORKOUT MODEL***
The workout model will contain the following methods, paths, and endpoints.

List all workouts:
    Endpoint path: /api/workouts
    Endpoint method: GET
    Headers: authorization: Bearer token
    Request body: JSON, no body in request

    Return: Status 200
    Return body: JSON

{
MurphWorkouts: [
                    {
                        "id ": 1,
                        "user_name" "Frank",
                        "date": "2024/01/11 09:00",
                        "total_duration": "time(int)
                    },
                    {
                        "id ": 2,
                        "user_name" "Joey",
                        "date": "2024/02/13 09:45",
                        "total_duration": "time(int)
                    }
                ]
}

List all workouts for a specific user:
    Endpoint path: /api/workouts/mine
    Endpoint method: GET
    Headers: authorization: Bearer token
    Request body: JSON, no body in request

    Return: Status 200
    Return body: JSON

{
MurphWorkouts: [
                    {
                        "id ": 1,
                        "user_name" "Frank",
                        "date": "2024/01/11 09:00",
                        "total_duration": "time(int)"
                    },
                    {
                        "id ": 2,
                        "user_name" "Frank",
                        "date": "2024/02/13 09:45",
                        "total_duration": "time(int)"
                    }
                ]
}

Show a single workout:
    Endpoint path: /api/workouts/{ID}
    Endpoint method: GET
    Headers: authorization: Bearer token
    Request body: JSON, no body in request
    
    Response: Status 200
    Response body: JSON (integers in milliseconds)

    {
        "id ": 1,
        "user_id" 2,
        "date": "2024/01/11 09:00",
        "run_1": 420000,
        "set_1": 420000,
        "set_2": 390000,
        "set_3": 380000,
        "set_4": 410000,
        "set_5": 425000,
        "set_6": 429000,
        "set_7": 520000,
        "set_8": 550000,
        "set_9": 555000,
        "set_10": 559000,
        "run_2": 450000,
        "total_duration": 5508000,
        "completed": true
    }

Create workout:
    Endpoint path: /api/workouts
    Endpoint method: POST
    Headers: authorization: Bearer token
    Request body: JSON
    
    {
        "user_id" 2,
        "date": "2024/01/11 09:00",
        "run_1": 420000,
        "set_1": 420000,
        "set_2": 390000,
        "set_3": 380000,
        "set_4": 410000,
        "set_5": 425000,
        "set_6": 429000,
        "set_7": 520000,
        "set_8": 550000,
        "set_9": 555000,
        "set_10": 559000,
        "run_2": 450000,
        "total_duration": 5508000,
        "completed": true
    }
    
    Response: Status 200
    Response body: JSON (integers in milliseconds)

    {
        "id ": 1,
        "user_id" 2,
        "date": "2024/01/11 09:00",
        "run_1": 420000,
        "set_1": 420000,
        "set_2": 390000,
        "set_3": 380000,
        "set_4": 410000,
        "set_5": 425000,
        "set_6": 429000,
        "set_7": 520000,
        "set_8": 550000,
        "set_9": 555000,
        "set_10": 559000,
        "run_2": 450000,
        "total_duration": 5508000,
        "completed": true
    }

***Show the leaderboard***
The leaderboard will show the best performances on the Murph workout, in descending order. Users can appear multiple times on the leaderboard, "arcade-style".

* endpoint path: /api/workouts/leaderboard
* Endpoint method: GET
* Headers: authorization: Bearer token
* Request body: n/a
* Response body:
{
    MurphWorkouts: [
                    {
                        "id ": "145",
                        "user_id" "10",
                        "date": "2024/01/11 09:00",
                        "run_1": 260000,
                        "set_1": 80000,
                        "set_2": 76380,
                        "set_3": 78420,
                        "set_4": 69420,
                        "set_5": 72400,
                        "set_6": 78500,
                        "set_7": 75890,
                        "set_8": 79127,
                        "set_9": 82521,
                        “set_10”: 83196,
                        "run_2": 320000,
                        "total_duration": 5508000,
                        "completed": true
                    },
                    {
                        "id ": "420",
                        "user_id" "10",
                        "date": "2024/01/11 09:00",
                        "run_1": 260000,
                        "set_1": 80000,
                        "set_2": 76380,
                        "set_3": 78420,
                        "set_4": 69420,
                        "set_5": 72400,
                        "set_6": 78500,
                        "set_7": 75890,
                        "set_8": 79127,
                        "set_9": 82521,
                        “set_10”: 83196,
                        "run_2": 320000,
                        "total_duration": 5508000,
                        "completed": true
                    },
                    {
                        "id ": "256",
                        "user_id" "10",
                        "date": "2024/01/11 09:00",
                        "run_1": 260000,
                        "set_1": 80000,
                        "set_2": 76380,
                        "set_3": 78420,
                        "set_4": 69420,
                        "set_5": 72400,
                        "set_6": 78500,
                        "set_7": 75890,
                        "set_8": 79127,
                        "set_9": 82521,
                        “set_10”: 83196,
                        "run_2": 320000,
                        "total_duration": 5508000,
                        "completed": true
                    }
                ]
}

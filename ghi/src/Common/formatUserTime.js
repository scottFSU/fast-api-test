// this takes milliseconds and return HH:MM:SS
function formatUserTime(time) {
    let hours = Math.floor(time / 3600)
    let minutes = Math.floor((time % 3600) / 60)
    let seconds = parseInt(time % 60)
    hours = String(hours).padStart(2, "0")
    minutes = String(minutes).padStart(2, "0")
    seconds = String(seconds).padStart(2, "0")
    return `${hours}:${minutes}:${seconds}`
}

export default formatUserTime

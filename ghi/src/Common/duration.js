const getDuration = (workout) => {
    return (workout.run_1 + workout.set_1 + workout.set_2 +  workout.set_3 + 
        workout.set_4 + workout.set_5 + workout.set_6 + workout.set_7 + 
        workout.set_8 + workout.set_9 + workout.set_10 + workout.run_2)
    }

export default getDuration

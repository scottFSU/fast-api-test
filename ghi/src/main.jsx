//@ts-check
import React from 'react'
import ReactDOM from 'react-dom/client'
import { AuthProvider } from '@galvanize-inc/jwtdown-for-react'
import './index.css'
import App from './App.jsx'


const API_HOST = import.meta.env.VITE_API_HOST
if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <AuthProvider baseUrl={API_HOST}>
            <div className="bg-gradient-to-t from-custom-orange to-orange-400 min-h-screen">
                <App />
            </div>
        </AuthProvider>
    </React.StrictMode>
)

export { API_HOST }

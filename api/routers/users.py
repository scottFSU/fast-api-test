from authenticator import authenticator
from fastapi import (
    APIRouter,
    Depends,
    status,
    HTTPException,
    Request,
    Response,
)
from typing import (
    Union,
)
from queries.users import UsersQueries
from models.models import (
    Error,
    UserIn,
    UserOut,
    AccountForm,
    AccountToken,
    UserOutWithHashedPassword
)


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    user: UserOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if user and authenticator.cookie_name in request.cookies:
        return AccountToken(
            access_token=request.cookies[authenticator.cookie_name],
            token_type="Bearer",
            user=user
            )


@router.post("/api/users", response_model=Union[AccountToken, Error])
async def create_user(
    info: UserIn,
    request: Request,
    response: Response,
    repo: UsersQueries = Depends(),
):
    if info.verified_password != info.password:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Passwords no not match:",
        )
    hashed_password = authenticator.hash_password(info.password)
    try:
        newuser = repo.create(info, hashed_password)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e),
        )
    if not isinstance(newuser, UserOutWithHashedPassword):
        return Error(message="Could not create user.")

    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(user=newuser, **token.dict())

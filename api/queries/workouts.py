from models.models import WorkoutIn, WorkoutOut, Error, LeaderboardOut
from queries.pool import pool
from typing import List, Union


class WorkoutsQueries:
    def create(
            self,
            workout: WorkoutIn,
    ):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO workouts
                        (user_id,
                        date,
                        run_1,
                        set_1,
                        set_2,
                        set_3,
                        set_4,
                        set_5,
                        set_6,
                        set_7,
                        set_8,
                        set_9,
                        set_10,
                        run_2,
                        is_completed)
                    VALUES
                        (
                        %s,%s,%s,%s,
                        %s,%s,%s,%s,
                        %s,%s,%s,%s,
                        %s,%s,%s
                        )
                    RETURNING id, *;
                    """,
                    [
                        workout.user_id,
                        workout.date,
                        workout.run_1,
                        workout.set_1,
                        workout.set_2,
                        workout.set_3,
                        workout.set_4,
                        workout.set_5,
                        workout.set_6,
                        workout.set_7,
                        workout.set_8,
                        workout.set_9,
                        workout.set_10,
                        workout.run_2,
                        workout.is_completed
                    ]
                )
                new_workout = {}
                new_workout["id"] = result.fetchone()[0]
                new_workout = WorkoutOut(**new_workout, **workout.dict())
                return new_workout

    def get_single_workout_by_id(
            self,
            id: int,
            user_id: int
    ) -> Union[WorkoutOut, None]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT *
                    FROM workouts
                    WHERE id = %s
                    AND user_id = %s
                    """,
                    [id, user_id]
                )
                record = db.fetchone()
                if record:
                    workout = WorkoutOut(
                        id=record[0],
                        user_id=record[1],
                        date=record[2],
                        run_1=record[3],
                        set_1=record[4],
                        set_2=record[5],
                        set_3=record[6],
                        set_4=record[7],
                        set_5=record[8],
                        set_6=record[9],
                        set_7=record[10],
                        set_8=record[11],
                        set_9=record[12],
                        set_10=record[13],
                        run_2=record[14],
                        is_completed=record[15],
                    )
                    return workout

    def get_all_user_workouts(
            self,
            user_id: int
    ) -> Union[Error, List[WorkoutOut]]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT *
                    FROM workouts
                    WHERE user_id = %s
                    ORDER BY id desc;
                    """,
                    [user_id]
                )
                workout_list = []
                for record in db:
                    workout = WorkoutOut(
                       id=record[0],
                       user_id=record[1],
                       date=record[2],
                       run_1=record[3],
                       set_1=record[4],
                       set_2=record[5],
                       set_3=record[6],
                       set_4=record[7],
                       set_5=record[8],
                       set_6=record[9],
                       set_7=record[10],
                       set_8=record[11],
                       set_9=record[12],
                       set_10=record[13],
                       run_2=record[14],
                       is_completed=record[15],
                    )
                    workout_list.append(workout)
                return workout_list

    def get_leaderboard(self) -> Union[Error, List[LeaderboardOut]]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT
                        workouts.*,
                        (run_1 + set_1 + set_2 + set_3 + set_4 + set_5 +
                        set_6 + set_7 + set_8 + set_9 + set_10 + run_2)
                        as duration,
                        users.username
                    FROM workouts
                    LEFT JOIN users on users.id = workouts.user_id
                    WHERE is_completed is true
                    ORDER BY duration ASC
                    LIMIT 100
                    """
                )
                leaderboard = []
                for record in db:
                    workout = LeaderboardOut(
                       id=record[0],
                       user_id=record[1],
                       date=record[2],
                       run_1=record[3],
                       set_1=record[4],
                       set_2=record[5],
                       set_3=record[6],
                       set_4=record[7],
                       set_5=record[8],
                       set_6=record[9],
                       set_7=record[10],
                       set_8=record[11],
                       set_9=record[12],
                       set_10=record[13],
                       run_2=record[14],
                       is_completed=record[15],
                       duration=record[16],
                       username=record[17]
                    )
                    leaderboard.append(workout)
                return leaderboard

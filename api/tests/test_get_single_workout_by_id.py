from fastapi.testclient import TestClient
from main import app
from queries.workouts import WorkoutsQueries
from models.models import WorkoutOut
from authenticator import authenticator
from datetime import date


client = TestClient(app)


class MockWorkoutQueries:
    """
    Mock version of the workouts queries from `api/queries/workouts.py`
    """
    def get_single_workout_by_id(self, id: int, user_id: int) -> WorkoutOut:
        return (
            WorkoutOut(
                id=129,
                user_id=1,
                date=date(2024, 1, 1),
                run_1=150000,
                set_1=60000,
                set_2=60000,
                set_3=60000,
                set_4=60000,
                set_5=60000,
                set_6=60000,
                set_7=60000,
                set_8=60000,
                set_9=60000,
                set_10=60000,
                run_2=150000,
                is_completed=True,
            )
        )


def mock_get_current_account_data():
    """
    Mocking get current account data
    """
    return {"id": 1}


def test_get_single_workout_by_id():
    """
    Test Function to GET a single workout by its ID
    """
    app.dependency_overrides[
            authenticator.get_current_account_data
        ] = mock_get_current_account_data
    app.dependency_overrides[WorkoutsQueries] = MockWorkoutQueries
    response = client.get("/api/workouts/129")
    assert response.status_code == 200
    workouts = response.json()
    assert len(workouts) == 16

    app.dependency_overrides = {}
